import 'package:bloc/bloc.dart';

part 'change_img.dart';

class ChangeStateCubit extends Cubit<ChangeImg> {
  static String src1 = 'https://developers.google.com/static/learn/images/flutter/flutter_logo_1920.jpg';
  static String src2 = 'https://static1.xdaimages.com/wordpress/wp-content/uploads/2018/02/Flutter-Framework-Feature-Image-Background-Colour.png?q=50&fit=contain&w=1140&h=&dpr=1.5';

  ChangeStateCubit() : super(ChangeImg(src: src1));
  bool pressTrack = true;

  void alterImg() {
    if (pressTrack) {
      emit(ChangeImg(src:state.src=src1));
    }
    else
      {
        emit(ChangeImg(src:state.src=src2));

      }
    pressTrack = !pressTrack;
  }

}
