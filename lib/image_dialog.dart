import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'change_state_cubit.dart';

class ImageDialog extends StatelessWidget {

  const ImageDialog( {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        height: 400,
        width: 400,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            const Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  "Welcome Here!!",
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight:FontWeight.bold,
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ClipOval(
                    child: BlocBuilder<ChangeStateCubit, ChangeImg>(
                        builder: (context,state)
                        {
                          return Image.network(
                              state.src,
                              width: 100,
                              height: 100,
                              fit: BoxFit.cover);

                        }
                    )
                ),
              ],
            ),
            Card(
              elevation: 5,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  decoration: const InputDecoration(
                      border: UnderlineInputBorder(),
                      labelText: 'Search your favorite pic',
                      labelStyle: TextStyle(
                        fontSize: 25,

                      )
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}