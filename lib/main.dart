import 'package:bloc_provider_application/change_state_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'image_dialog.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ChangeStateCubit(),
      child: MaterialApp(
        title: 'Flutter Demo',
        home: TestPage(),
      ),
    );
  }
}
class TestPage extends StatefulWidget {



  @override
  State<TestPage> createState() => TestPageState();
}

class TestPageState extends State<TestPage> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(child: Text("Learn Flutter")),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: InkWell(
              onTap: () {
                //print("Hi");
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return ImageDialog();
                    });
              },
              child: BlocBuilder<ChangeStateCubit, ChangeImg>(
                builder: (context,state)
                {
                return Image.network(
                    state.src,
                    width: 100,
                    height: 100,
                    fit: BoxFit.cover);

                }
              )

              // child: Image.network(
              //     src,
              //     width: 100,
              //     height: 100,
              //     fit: BoxFit.cover)
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: () {
                  BlocProvider.of<ChangeStateCubit>(context).alterImg();
                },
                child: const Text("Click me"),
              ),
            ],
          ),
        ],
      ),
    );
  }
}